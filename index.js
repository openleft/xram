import { Client, Events, GatewayIntentBits } from "discord.js"
import "dotenv/config"

const { DISCORD_TOKEN } = process.env

const client = new Client({ intents: [GatewayIntentBits.Guilds] })

client.once(Events.ClientReady, c => {
    console.log(`Ready! Logged in as ${c.user.tag}`)
})

client.login(DISCORD_TOKEN)